/* eslint-disable */

import axios from 'axios';

module.exports = async function() {
  // Configure axios for tests to use.
  const host = process.env.HOST ?? 'localhost';
  const port = process.env.PORT ?? '3022';
  axios.defaults.baseURL = `http://${ host }:${ port }`;
  console.log(`Using axios base URL: ${ axios.defaults.baseURL }`);
};
