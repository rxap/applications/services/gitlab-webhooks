import {
  Body,
  Controller,
  Inject,
  Logger,
  Post,
  UseGuards
} from '@nestjs/common';
import { ToDtoInstance } from '@rxap/nest-dto';
import { GitlabEventUuid } from './decotators/gitlab-event-uuid.decorator';
import { GitlabEvent } from './decotators/gitlab-event.decorator';
import { GitlabInstance } from './decotators/gitlab-instance.decorator';
import { GitlabToken } from './decotators/gitlab-token.decorator';
import { GitlabVersion } from './decotators/gitlab-version.decorator';
import { GitlabWebhookUuid } from './decotators/gitlab-webhook-uuid.decorator';
import {
  IWebhookEvent,
  WebhookMetadata
} from './dtos/webhook-event';
import { EventTypes } from './event-types';
import { WebhookGuard } from './webhook.guard';
import { WebhookService } from './webhook.service';

@Controller()
@UseGuards(WebhookGuard)
export class WebhookController {

  @Inject(WebhookService)
  private readonly webhookService: WebhookService;

  @Inject(Logger)
  private readonly logger: Logger;

  @Post()
  hook(
    @Body() body: IWebhookEvent & Record<string, unknown>,
    @GitlabWebhookUuid() webhookUuid: string,
    @GitlabEventUuid() eventUuid: string,
    @GitlabInstance() instance: string,
    @GitlabVersion() instanceVersion: string,
    @GitlabEvent() event: EventTypes,
    @GitlabToken() token: string
  ) {
    this.logger.verbose('Received webhook event %JSON', body, 'WebhookController');
    const plainMetadata = {
      event,
      eventUuid,
      instance,
      instanceVersion,
      webhookUuid,
      token
    };
    this.logger.verbose('Webhook metadata %JSON', plainMetadata, 'WebhookController');
    const metadata = ToDtoInstance(WebhookMetadata, plainMetadata);
    return this.webhookService.add(body, metadata);
  }

}
