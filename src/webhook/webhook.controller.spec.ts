import { Logger } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import {
  Test,
  TestingModule
} from '@nestjs/testing';
import { WebhookController } from './webhook.controller';
import { WebhookService } from './webhook.service';

describe('WebhookController', () => {
  let controller: WebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          load: [
            () => (
              {}
            )
          ]
        })
      ],
      controllers: [ WebhookController ],
      providers: [ WebhookService, Logger ]
    }).overrideProvider(WebhookService).useValue({ add: () => Promise.resolve(true) }).compile();

    controller = module.get<WebhookController>(WebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
