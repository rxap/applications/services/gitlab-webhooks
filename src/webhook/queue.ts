/**
 * A class representing a queue data structure.
 * @template T The type of elements stored in the queue.
 */
export class Queue<T> {

  private storage: T[] = [];

  /**
   * Constructs a new instance of the class.
   *
   * @param {Array} initialElements - An array of initial elements to be stored.
   */
  constructor(initialElements: T[] = []) {
    this.storage = initialElements;
  }

  /**
   * Check if storage is empty.
   *
   * @return {boolean} - True if storage is empty, false otherwise.
   */
  get isEmpty(): boolean {
    return this.storage.length === 0;
  }

  /**
   * Returns the size of the storage.
   *
   * @returns {number} - The size of the storage.
   */
  get size(): number {
    return this.storage.length;
  }

  /**
   * Adds an element to the end of the queue.
   *
   * @param {T} element - The element to be added to the queue.
   * @return {void}
   */
  enqueue(element: T): void {
    this.storage.push(element);
  }

  /**
   * Removes and returns the element at the front of the queue.
   *
   * @return {T|undefined} The element that was removed from the front of the queue, or undefined if the queue is empty.
   */
  dequeue(): T | undefined {
    if (this.isEmpty) {
      return undefined;
    }
    return this.storage.shift();
  }

  /**
   * Retrieves the element at the top of the storage without removing it.
   *
   * @returns {T | undefined} The element at the top of the storage, or undefined if the storage is empty.
   */
  peek(): T | undefined {
    return this.storage[0];
  }

  /**
   * Clears the storage array.
   *
   * @return {void}
   */
  clear(): void {
    this.storage = [];
  }

}
