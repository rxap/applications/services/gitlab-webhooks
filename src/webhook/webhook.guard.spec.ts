import { WebhookGuard } from './webhook.guard';

describe('WebhookGuard', () => {
  it('should be defined', () => {
    expect(new WebhookGuard()).toBeDefined();
  });
});
