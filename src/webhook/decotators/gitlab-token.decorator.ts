import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext
} from '@nestjs/common';
import { Request } from 'express';

export function GitlabTokenFromExecutionContext(ctx: ExecutionContext): string {
  const request = ctx.switchToHttp().getRequest<Request>();
  const gitlabToken = request.headers['x-gitlab-token'];
  if (!gitlabToken) {
    throw new BadRequestException('Missing X-Gitlab-Token header');
  }
  if (typeof gitlabToken !== 'string') {
    throw new BadRequestException('Invalid X-Gitlab-Token header');
  }
  return gitlabToken;
}

export const GitlabToken = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    return GitlabTokenFromExecutionContext(ctx);
  }
);
