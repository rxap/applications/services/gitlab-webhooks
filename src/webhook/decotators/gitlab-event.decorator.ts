import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext
} from '@nestjs/common';
import { Request } from 'express';
import { IsEventType } from '../event-types';

export const GitlabEvent = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest<Request>();
    const event = request.headers['x-gitlab-event'];
    if (!event) {
      throw new BadRequestException('Missing X-Gitlab-Event header');
    }
    if (typeof event !== 'string') {
      throw new BadRequestException('Invalid X-Gitlab-Event header');
    }
    const match = event.match(/^([a-zA-z\s]+) Hook$/);
    if (!match) {
      throw new BadRequestException('Gitlab event is not a valid event');
    }
    const eventName = match[1];
    if (!IsEventType(eventName)) {
      throw new BadRequestException('Gitlab event is not a valid event');
    }
    return eventName;
  }
);
