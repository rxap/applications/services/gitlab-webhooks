import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext
} from '@nestjs/common';
import { isUUID } from 'class-validator';
import { Request } from 'express';

export const GitlabWebhookUuid = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest<Request>();
    const webhookUUID = request.headers['x-gitlab-webhook-uuid'];
    if (!webhookUUID) {
      throw new BadRequestException('Missing X-Gitlab-Webhook-UUID header');
    }
    if (typeof webhookUUID !== 'string') {
      throw new BadRequestException('Invalid X-Gitlab-Webhook-UUID header');
    }
    if (!isUUID(webhookUUID)) {
      throw new BadRequestException('Gitlab webhook uuid is not a valid UUID');
    }
    return webhookUUID;
  }
);
