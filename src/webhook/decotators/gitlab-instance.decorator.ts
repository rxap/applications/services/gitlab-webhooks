import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext
} from '@nestjs/common';
import { Request } from 'express';

export function GitlabInstanceFromExecutionContext(ctx: ExecutionContext): string {
  const request = ctx.switchToHttp().getRequest<Request>();
  const gitlabInstance = request.headers['x-gitlab-instance'];
  if (!gitlabInstance) {
    throw new BadRequestException('Missing X-Gitlab-Instance header');
  }
  if (typeof gitlabInstance !== 'string') {
    throw new BadRequestException('Invalid X-Gitlab-Instance header');
  }
  const match = gitlabInstance.match(/^https:\/\/.+$/);
  if (!match) {
    throw new BadRequestException('Invalid X-Gitlab-Instance url');
  }
  return gitlabInstance;
}

export const GitlabInstance = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    return GitlabInstanceFromExecutionContext(ctx);
  }
);
