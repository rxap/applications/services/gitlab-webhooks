import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext
} from '@nestjs/common';
import { isUUID } from 'class-validator';
import { Request } from 'express';

export const GitlabEventUuid = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest<Request>();
    const eventUUID = request.headers['x-gitlab-event-uuid'];
    if (!eventUUID) {
      throw new BadRequestException('Missing X-Gitlab-Event-UUID header');
    }
    if (typeof eventUUID !== 'string') {
      throw new BadRequestException('Invalid X-Gitlab-Event-UUID header');
    }
    if (!isUUID(eventUUID)) {
      throw new BadRequestException('Gitlab event uuid is not a valid UUID');
    }
    return eventUUID;
  }
);
