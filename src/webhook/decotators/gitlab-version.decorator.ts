import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext
} from '@nestjs/common';
import { Request } from 'express';

export const GitlabVersion = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest<Request>();
    const userAgent = request.headers['user-agent'];
    if (!userAgent) {
      throw new BadRequestException('Missing User-Agent header');
    }
    const match = userAgent.match(/^GitLab\/(\d+\.\d+\.\d+.*)$/);
    if (!match) {
      throw new BadRequestException('Invalid User-Agent header');
    }
    return match[1];
  }
);
