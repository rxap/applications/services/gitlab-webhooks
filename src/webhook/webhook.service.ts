import {
  Inject,
  Injectable,
  Logger
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ToDtoInstance } from '@rxap/nest-dto';
import {
  ClientRMQExchange,
  RABBITMQ_EXCHANGE
} from '@rxap/nest-rabbitmq';
import { classTransformOptions } from '@rxap/nest-utilities';
import { instanceToPlain } from 'class-transformer';
import { firstValueFrom } from 'rxjs';
import {
  AssertMergeRequestEvent,
  MergeRequestEvent
} from './dtos/merge-request-event';
import {
  IWebhookEvent,
  WebhookEvent,
  WebhookMetadata
} from './dtos/webhook-event';
import 'colors';

@Injectable()
export class WebhookService {

  @Inject(Logger)
  private readonly logger: Logger;

  @Inject(RABBITMQ_EXCHANGE)
  private readonly client: ClientRMQExchange;

  @Inject(ConfigService)
  private readonly config: ConfigService;

  add(body: IWebhookEvent & Record<string, unknown>, metadata: WebhookMetadata): Promise<boolean> {
    let webhook: WebhookEvent;
    switch (body.object_kind) {

      case 'merge_request':
        AssertMergeRequestEvent(body);
        this.logger.log('Received merge request event', 'WebhookService');
        webhook = ToDtoInstance(MergeRequestEvent, {
          ...body,
          metadata
        });
        break;

      case undefined:
      case null:
      case '':
        this.logger.error('Received empty object kind', 'WebhookService');
        return Promise.resolve(false);

      default:
        this.logger.warn(`Unsupported object kind: ${ body.object_kind }`, 'WebhookService');
        webhook = ToDtoInstance(WebhookEvent, {
          ...body,
          metadata
        });
        break;

    }
    this.logger.debug('Emit webhook event %JSON', instanceToPlain(webhook, classTransformOptions), 'WebhookService');
    return this.publish(webhook);
  }

  publish(webhook: WebhookEvent) {
    const exchange = this.config.getOrThrow('RABBITMQ_EXCHANGE');
    const routingKey = webhook.safeRoutingKey;
    this.logger.log(
      `Publishing webhook event to exchange '${ exchange.blue }' with routing key '${ routingKey.blue }'`,
      'WebhookService'
    );
    return firstValueFrom(this.client.emit(
      routingKey,
      webhook
    ));
  }

}
