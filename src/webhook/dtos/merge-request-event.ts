import {
  Expose,
  Type
} from 'class-transformer';
import {
  Equals,
  IsInstance,
  IsNumber,
  IsString,
  Min
} from 'class-validator';
import { Project } from './project';
import {
  IWebhookEvent,
  WebhookEvent
} from './webhook-event';

export class MergeRequestAttributes {

  @Expose()
  @IsNumber()
  @Min(1)
  id!: number;

  @Expose()
  @IsNumber()
  @Min(1)
  iid!: number;

  @Expose()
  @IsString()
  target_branch!: string;

  @Expose()
  @IsString()
  source_branch!: string;

}

export class MergeRequestEvent extends WebhookEvent {

  @Expose()
  @IsString()
  @Equals('merge_request')
  object_kind!: 'merge_request';

  @Expose()
  @IsString()
  @Equals('merge_request')
  event_type!: 'merge_request';

  @Expose()
  @IsInstance(Project)
  @Type(() => Project)
  project!: Project;

  @Expose()
  @IsInstance(MergeRequestAttributes)
  @Type(() => MergeRequestAttributes)
  object_attributes!: MergeRequestAttributes;

}

export function IsMergeRequestEvent(object: IWebhookEvent): object is MergeRequestEvent {
  return object.object_kind === 'merge_request';
}

export function AssertMergeRequestEvent(object: IWebhookEvent): asserts object is MergeRequestEvent {
  if (!IsMergeRequestEvent(object)) {
    throw new Error('The object is not a merge request event');
  }
}
