import {
  Expose,
  Type
} from 'class-transformer';
import {
  IsEnum,
  IsInstance,
  IsSemVer,
  IsString,
  IsUrl,
  IsUUID
} from 'class-validator';
import { EventTypes } from '../event-types';

export class WebhookMetadata {

  @Expose()
  @IsEnum(EventTypes)
  event!: EventTypes;

  @Expose()
  @IsUUID()
  eventUuid!: string;

  @Expose()
  @IsUrl()
  instance!: string;

  @Expose()
  @IsSemVer()
  instanceVersion!: string;

  @Expose()
  @IsUUID()
  webhookUuid!: string;

  @Expose()
  @IsString()
  token!: string;

  get normalizedInstance(): string {
    return this.instance.replace(/https?:\/\//, '').replace(/\./g, '_');
  }

}

export interface IWebhookEvent {
  object_kind: string;
}

export class WebhookEvent implements IWebhookEvent {

  @Expose()
  @IsInstance(WebhookMetadata)
  @Type(() => WebhookMetadata)
  metadata!: WebhookMetadata;

  @Expose()
  @IsString()
  object_kind!: string;

  /**
   * Returns the routing key for a specific event.
   *
   * @return {string} The routing key generated based on metadata and object_kind.
   */
  get routingKey(): string {
    return `${ this.metadata.normalizedInstance }.${ this.metadata.webhookUuid }.${ this.object_kind }.${ this.metadata.eventUuid }`;
  }

  /**
   * Limits the routing key to 255 bytes
   *
   * @returns {string} The safe routing key limited to 255 bytes
   */
  get safeRoutingKey(): string {
    return this.routingKey.slice(0, 255);
  }

}
