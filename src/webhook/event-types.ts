export enum EventTypes {
  PUSH = 'Push',
  TAG = 'Tag Push',
  ISSUE = 'Issue',
  COMMENT = 'Note',
  MERGE_REQUEST = 'Merge Request',
  WIKI_PAGE = 'Wiki Page',
  PIPELINE = 'Pipeline',
  JOB = 'Job',
  DEPLOYMENT = 'Deployment',
  FEATURE_FLAG = 'Feature Flag',
  RELEASE = 'Release',
  EMOJI = 'Emoji',
  RESOURCE_ACCESS_TOKEN = 'Resource Access Token',
  SUBGROUP = 'Subgroup',
  GROUP_MEMBER = 'Member',
}

export function IsEventType(event: string): event is EventTypes {
  return Object.values(EventTypes).includes(event as EventTypes);
}
