import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  Logger
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Observable } from 'rxjs';
import { GitlabInstanceFromExecutionContext } from './decotators/gitlab-instance.decorator';
import { GitlabTokenFromExecutionContext } from './decotators/gitlab-token.decorator';

@Injectable()
export class WebhookGuard implements CanActivate {

  @Inject(ConfigService)
  private readonly config: ConfigService;

  @Inject(Logger)
  private readonly logger: Logger;

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {

    const gitlabToken = GitlabTokenFromExecutionContext(context);
    this.logger.debug(`Received Gitlab token from context '${ gitlabToken }'`, 'WebhookGuard');
    const instanceUrl = GitlabInstanceFromExecutionContext(context);
    this.logger.debug(`Received Gitlab instance URL from context '${ instanceUrl }'`, 'WebhookGuard');

    this.logger.error('Gitlab token and instance URL are not validated!', 'WebhookGuard');

    return true;
  }
}
