import {
  Controller,
  Get,
  Inject
} from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService
} from '@nestjs/terminus';
import { RabbitmqHealthIndicator } from '@rxap/nest-rabbitmq';
import { Public } from '@rxap/nest-utilities';
import { VaultHealthIndicator } from '@rxap/nest-vault';

@Controller('health')
@Public()
@ApiExcludeController()
export class HealthController {
  @Inject(HealthCheckService)
  private readonly health: HealthCheckService;

  @Inject(VaultHealthIndicator)
  private readonly vaultHealthIndicator: VaultHealthIndicator;
  @Inject(RabbitmqHealthIndicator)
  private readonly rabbitmqHealthIndicator: RabbitmqHealthIndicator;

  @Get()
  @HealthCheck()
  public healthCheck(): Promise<HealthCheckResult> {
    return this.health.check([
      async () => this.vaultHealthIndicator.isHealthy(),
      async () => this.rabbitmqHealthIndicator.isHealthy()
    ]);
  }

  @Get('vault')
  @HealthCheck()
  public vault(): Promise<HealthCheckResult> {
    return this.health.check([
      async () => this.vaultHealthIndicator.isHealthy()
    ]);
  }

  @Get('rabbitmq')
  @HealthCheck()
  public rabbitmq(): Promise<HealthCheckResult> {
    return this.health.check([
      async () => this.rabbitmqHealthIndicator.isHealthy()
    ]);
  }
}
