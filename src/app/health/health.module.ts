import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { RabbitmqHealthIndicator } from '@rxap/nest-rabbitmq';
import { VaultHealthIndicator } from '@rxap/nest-vault';
import { HealthController } from './health.controller';

@Module({
  imports: [ TerminusModule ],
  providers: [ VaultHealthIndicator, RabbitmqHealthIndicator ],
  controllers: [ HealthController ]
})
export class HealthModule {}
