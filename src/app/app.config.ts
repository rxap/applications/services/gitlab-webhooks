import { GenerateRandomString } from '@rxap/utilities';
import * as Joi from 'joi';
import { SchemaMap } from 'joi';
import * as process from 'node:process';
import { environment } from '../environments/environment';

const validationSchema: SchemaMap = {};
validationSchema['COOKIE_SECRET'] = Joi.string().default(
  GenerateRandomString()
);
validationSchema['THROTTLER_LIMIT'] = Joi.string().default(10);
validationSchema['THROTTLER_TTL'] = Joi.string().default(1);
validationSchema['GLOBAL_API_PREFIX'] =
  Joi.string().default('/webhooks/gitlab');
validationSchema['PORT'] = Joi.number().default(3022);
validationSchema['SENTRY_DEBUG'] = Joi.string().default(
  environment.sentry?.debug ?? false
);
validationSchema['SENTRY_SERVER_NAME'] = Joi.string().default(
  process.env.ROOT_DOMAIN ?? environment.app
);
validationSchema['SENTRY_RELEASE'] = Joi.string();
validationSchema['SENTRY_ENVIRONMENT'] = Joi.string();
validationSchema['SENTRY_ENABLED'] = Joi.string().default(
  environment.sentry?.enabled ?? false
);
validationSchema['SENTRY_DSN'] = Joi.string();

validationSchema['VAULT_SKIP_VERIFY'] = Joi.boolean().default(false);
validationSchema['VAULT_ADDR'] = Joi.string().default('https://vault-ui.vault.svc.cluster.local:8200');
validationSchema['VAULT_TOKEN'] = Joi.string();
validationSchema['VAULT_DISABLED'] = Joi.boolean().default(!!process.env.CI);
validationSchema['VAULT_KUBERNETES_ROLE'] = Joi.string().default('rxap');
validationSchema['VAULT_KUBERNETES_AUTO_RENEW'] = Joi.boolean().default(true);

validationSchema['RABBITMQ_HOST'] = Joi.string().default('rabbitmq.rabbitmq.svc.cluster.local');
validationSchema['RABBITMQ_PORT'] = Joi.number().default(5672);
validationSchema['RABBITMQ_VHOST'] = Joi.string().default('/');
validationSchema['RABBITMQ_USERNAME'] = Joi.string();
validationSchema['RABBITMQ_PASSWORD'] = Joi.string();
validationSchema['RABBITMQ_VAULT_ROLE'] = Joi.string().default('rxap');
validationSchema['RABBITMQ_DISABLED'] = Joi.boolean().default(!!process.env.CI);
validationSchema['RABBITMQ_EXCHANGE'] = Joi.string().default('gitlab-events');

export const VALIDATION_SCHEMA = Joi.object(validationSchema);
