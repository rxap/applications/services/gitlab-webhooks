import { CacheModule } from '@nestjs/cache-manager';
import {
  HttpException,
  Logger,
  Module
} from '@nestjs/common';
import {
  ConfigModule,
  ConfigService
} from '@nestjs/config';
import {
  APP_GUARD,
  APP_INTERCEPTOR
} from '@nestjs/core';
import { ClientsModule } from '@nestjs/microservices';
import {
  ThrottlerGuard,
  ThrottlerModule
} from '@nestjs/throttler';
import {
  ClientRmqExchangeModuleOptionsFactory,
  RABBITMQ_EXCHANGE
} from '@rxap/nest-rabbitmq';
import {
  SENTRY_INTERCEPTOR_OPTIONS,
  SentryInterceptor,
  SentryModule,
  SentryOptionsFactory
} from '@rxap/nest-sentry';
import {
  CacheModuleOptionsLoader,
  ENVIRONMENT,
  GetLogLevels,
  ThrottlerModuleOptionsLoader
} from '@rxap/nest-utilities';
import {
  RabbitmqVaultService,
  VaultModule
} from '@rxap/nest-vault';
import { environment } from '../environments/environment';
import { WebhookModule } from '../webhook/webhook.module';
import { VALIDATION_SCHEMA } from './app.config';

import { AppController } from './app.controller';
import { HealthModule } from './health/health.module';

@Module({
  imports: [
    HealthModule,
    WebhookModule,
    ThrottlerModule.forRootAsync({
      useClass: ThrottlerModuleOptionsLoader
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: VALIDATION_SCHEMA
    }),
    CacheModule.registerAsync({
      isGlobal: true,
      useClass: CacheModuleOptionsLoader
    }),
    SentryModule.forRootAsync(
      {
        imports: [ ConfigModule ],
        inject: [ ConfigService ],
        useFactory: SentryOptionsFactory(environment)
      },
      {
        logLevels: GetLogLevels()
      }
    ),
    VaultModule.register(),
    ClientsModule.registerAsync({
      clients: [
        {
          name: RABBITMQ_EXCHANGE,
          useClass: ClientRmqExchangeModuleOptionsFactory,
          extraProviders: [ RabbitmqVaultService, Logger ]
        }
      ],
      isGlobal: true
    })
  ],
  controllers: [ AppController ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard
    },
    {
      provide: ENVIRONMENT,
      useValue: environment
    },
    {
      provide: SENTRY_INTERCEPTOR_OPTIONS,
      useValue: {
        filters: [
          {
            type: HttpException,
            filter: (exception: HttpException) => 500 > exception.getStatus()
          },
        ],
      },
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: SentryInterceptor
    },
  ],
})
export class AppModule {}
