import { Environment } from '@rxap/nest-utilities';

export const environment: Environment = {
  name: 'production',
  production: true,
  app: 'gitlab-webhooks',
  sentry: {
    enabled: true,
    debug: false
  }
};
