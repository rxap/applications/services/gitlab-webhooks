import { Environment } from '@rxap/nest-utilities';

export const environment: Environment = {
  name: 'swagger',
  production: true,
  swagger: true,
  app: 'gitlab-webhooks'
};
