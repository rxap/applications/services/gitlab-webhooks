# Changelog

## [0.0.1-nightly.7](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-nightly.6...v0.0.1-nightly.7) (2024-08-10)


### Bug Fixes

* update rabbitmq package ([14599db](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/14599db10bd4a6d6a516c79228de16b71901b6a8))

## [0.0.1-nightly.6](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-nightly.5...v0.0.1-nightly.6) (2024-08-08)


### Bug Fixes

* use vault and rabbitmq packages ([cd9a914](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/cd9a9145ceabb70fd39e0269b347a050f2a195e4))

## [0.0.1-nightly.5](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-nightly.4...v0.0.1-nightly.5) (2024-07-23)

## [0.0.1-nightly.4](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-nightly.3...v0.0.1-nightly.4) (2024-05-31)


### Bug Fixes

* **deps:** update dependency cache-manager to v5.5.3 ([ff89675](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/ff896757a75afb1eabbbc2dfd79ad59006626e72))

## [0.0.1-nightly.3](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-nightly.2...v0.0.1-nightly.3) (2024-05-30)


### Bug Fixes

* the increment value must be a string or empty ([28f42a3](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/28f42a3ccc85c274084f578d814925c44512e256))

## [0.0.1-nightly.2](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-nightly.1...v0.0.1-nightly.2) (2024-05-22)


### Features

* **health-check:** add vault and rabbitmq health indicators for monitoring service status ([8c726e4](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/8c726e44bf05383a33a3685484f1e0e3a77ebcec))

## [0.0.1-nightly.1](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-nightly.0...v0.0.1-nightly.1) (2024-05-20)


### Bug Fixes

* **webhook:** handle empty object kind in payload, emit generic webhook event ([486608f](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/486608ff5856dabe9140ba4436be8bfa7fe7a707))

## 0.0.1-nightly.0 (2024-05-19)


### Bug Fixes

* added missing check for Kubernetes role in Vault service. ([4325be5](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/4325be5759b74fd0c1591d58323096c29e97da3b))

## [0.0.1-edge.4](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-edge.3...v0.0.1-edge.4) (2024-05-18)

## [0.0.1-edge.3](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-edge.2...v0.0.1-edge.3) (2024-05-18)

## [0.0.1-edge.2](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-edge.1...v0.0.1-edge.2) (2024-05-16)


### Bug Fixes

* log the event metadata ([aa65170](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/aa6517036e3b003a6bd6b1643bff527be98aab34))

## [0.0.1-edge.1](https://gitlab.com/rxap/applications/services/gitlab-webhooks/compare/v0.0.1-edge.0...v0.0.1-edge.1) (2024-05-16)


### Bug Fixes

* improve logging ([021367f](https://gitlab.com/rxap/applications/services/gitlab-webhooks/commit/021367ffc8b0c6557acb5217e77f059508dd6009))

## 0.0.1-edge.0 (2024-05-16)
